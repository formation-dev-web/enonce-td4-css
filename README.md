CSS − TD4 : Responsive web design
=================================

En vous basant, au choix :

- sur votre travail du TD3
- ou sur [le corrigé du TD3](https://framagit.org/ecn-web-2016/correction-td3-css)

… Ajoutez des *media-queries* pour vous adapter à différentes tailles de
fenêtre (cf maquettes).

*NB: vu que nous avons commencé au TD3 par un design pour les desktop, il ne
 s'agit donc pas d'une approche mobile-first, qui demanderait de reprendre au début*

Consignes
---------

- Il n'est normalement **pas nécessaire de toucher au HTML** (si ça n'est pour
  la balise `meta viewport`).
- il est normalement possible de **toucher très peu au CSS existant** (on se
  contente d'ajouter des media-queries)
- la CSS issue du TD3 peut s'appliquer telle quelle dans la plage 801→1600px
  (pas de media-query nécessaire).

Conseils
--------

- Utiliser la « vue adaptative » du navigateur (outils de développement)
- Commencez par la taille native (801px→1600px), pour laquelle il n'y a rien à
  faire, puis écriver vos media-queries en "descendant" progressivement vers
  les petites tailles.
- N'oubliez pas les grandes tailles !
- L'application d'un voile sur l'image de fond du `<header>` en petite taille
  n'est pas aisée, gardez-vous ça pour la fin :-)

Maquettes (dans un premier temps)
----------------------------------

- minimun à 600px (min-width)
- [601px→800px](maquettes/600-800.png)
- [801px→1600px](maquettes/800-1600.png) (équivalent maquette TD3)
- maximum à 1600px (max-width)

 BONUS Maquettes (si vous avez le temps)
----------------------------------------

On ajoute quelques tailles d'écran…

- minimun à 320px (min-width)
- [320px→600px](maquettes/320-600.png) *nouveau*
- [601px→800px](maquettes/600-800.png)
- [801px→1600px](maquettes/800-1600.png) (équivalent maquette TD3)
- [1601px→2000px](maquettes/1600-2000.png) *nouveau*
- maximum à 2000px (max-width)

Super-BONUS (si vous avez le temps)
-----------------------------------

- Créer un « menu hamburger » pour la taille 320→600px
- Suggérer une amélioration (nécessitant un léger changement du HTML pour que
  le contenu des actuels `<p class="info">` soient plus élégants en-dessous de
  600px).
